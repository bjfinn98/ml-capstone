from data_adjuster import ImprovedChessDataset
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.colors import BoundaryNorm


fig = plt.figure()

data = ImprovedChessDataset("../data/lichess_db_puzzle.csv", convert=False)

themes = data.get_themes_dict()
data = data.get_representation_for_dffnn()
print(themes)
lab = []
for i in range(len(themes)):
    lab.append(themes[i])

num = [0] * len(themes)

for puzzle in data["Themes"]:
    for i, v in enumerate(puzzle):
        if v == 1:
            num[i] += 1

print(lab)
print(num)
ax = fig.add_axes([0,0,1,1])
ax.barh(lab, num)
plt.savefig("theme_distrib.png", bbox_inches="tight")


fig = plt.figure()
heatmap = np.zeros((8,8))
for board, theme in zip(data["Vec_Lin"], data["Themes"]):
    if theme[0] != 1: # solely for advantage puzzles
        continue
    for i in range(int(len(board) / 5)):
        curr_emb = board[(i * 6):((i+1) * 6)]
        if np.array_equal(curr_emb, [0,0,0,0,0,1]):  # black king
            heatmap[int(i / 8)][i % 8] -= 1
        if np.array_equal(curr_emb, [0,1,0,0,0,1]):  # white king
            heatmap[int(i / 8)][i % 8] += 1

# result of above code
"""
heatmap = np.array(
        [-3661, -20904, -44650, -12496, -144115, -32425, -442999, -77815,
        -2938, -6270, -10334, -18525, -25578, -30717, -56176, -43126,
        -968, -3333, -6060, -9905, -12515, -13885, -12289, -8041,
        -210, -537, -1722, -3762, -4384, -3977, -2370, -1379,
        146, 517, 1472, 3387, 4490, 3902, 2472, 1562,
        849, 2755, 4899, 10110, 13575, 13183, 1311, 7369,
        2742, 5676, 8658, 15445, 21515, 26681, 44681, 45519,
        4315, 28417, 53229, 9376, 122712, 30536, 464062, 90704]     
        ).reshape(8,8)
"""

cmap = plt.get_cmap("coolwarm")
cmaplist = [cmap(i) for i in range(cmap.N)]

cmap = cmap.from_list('Custom cmap', cmaplist, cmap.N)
bounds = np.arange(np.min(heatmap), np.max(heatmap), 10000)
index = np.searchsorted(bounds, 0) # going to force insert 0 into bounds
bounds = np.insert(bounds, index, 0)
norm = BoundaryNorm(bounds, cmap.N)
print(bounds)
plt.imshow(heatmap, interpolation='none', norm=norm, cmap=cmap)
plt.colorbar()
plt.title("Heatmap of Kings for Advantage Puzzles")
plt.savefig("AdvKingHeat.png", bbox_inches="tight")
