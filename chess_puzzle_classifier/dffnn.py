import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
import pandas
from data_adjuster import ImprovedChessDataset
import os

def main():
    test = DFFNNModel()
   
    dataset = ImprovedChessDataset("../data/lichess_db_puzzle.csv").get_representation_for_dffnn()
   
    if os.path.exists("dffnn.nn"):
        test.load_state_dict(torch.load("dffnn.nn"))
    else:
        for epoch in range(3):
            print(f"Starting epoch {epoch + 1}")
            test.train(dataset, verbose=True)
        torch.save(test.state_dict(), "dffnn.nn")
    
    print(dataset.head(10))
    pred = dataset["Vec_Lin"][0]
    print(test.predict(pred))
    res = dataset["Themes"][0]
    print(f"Expected: {res}")

class DFFNNModel(nn.Module):
    """
    Finalized implementation for the DFFNN model
    TO DO: Implement the methods specified in the evaluation script
    """
    def __init__(self): # array specifies sizes of hidden layers
        super(DFFNNModel, self).__init__()

        
        self.inp_layer = nn.Linear(64 * 6, 150) # Board size + size of vec_emb
        self.h1 = nn.Linear(150, 100)
        self.h2 = nn.Linear(100, 95)
        self.out = nn.Linear(95, 14)
        self.loss_fn = nn.MSELoss()
        self.optimizer = torch.optim.SGD(self.parameters(), lr=0.001)


    def __str__(self):
        return "DFFNNModel"

    def train(self, data, verbose=False):
        size = len(data)
        count = 0
        per = 0
        for x, y in zip(data["Vec_Lin"], data["Themes"]):
            x = torch.tensor(x).float()
            pred = self.__train(x).float()
            y = torch.tensor(y).float()
            loss = self.loss_fn(pred, y)

            self.optimizer.zero_grad()
            loss.backward()
            self.optimizer.step()
            count += 1

            if count % int((len(data)/100)) == 0 and count != 0 and verbose:
                per+=1
                print(f"{per}% done")
                print(f"loss:{loss.item()}")
                print(f"prediction : {pred}, actual: {y}")
                count = 0

    
    def __train(self, inp):
        inp = self.inp_layer(inp)
        inp = F.relu(inp)
        inp = self.h1(inp)
        inp = F.relu(inp)
        inp = self.h2(inp)
        inp = F.relu(inp)
        inp = self.out(inp)
        inp = F.relu(inp)

        return inp


    def predict(self, inp):
        inp = torch.tensor(inp).float()
       
        inp = self.inp_layer(inp)
        inp = F.relu(inp)
        inp = self.h1(inp)
        inp = F.relu(inp)
        inp = self.h2(inp)
        inp = F.relu(inp)
        inp = self.out(inp)
        inp = F.relu(inp)

        return F.log_softmax(inp, dim=0).detach().numpy()

if __name__ == "__main__":
    main()
