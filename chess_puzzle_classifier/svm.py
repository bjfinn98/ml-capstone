"""
Created By: Barak Finnegan
Date: 3/10/21
Purpose: Creating initial support vector machine to make classifications for our data set.
"""

from sklearn import svm
from data_adjuster import ImprovedChessDataset
import os
import pandas


def main():
    """
    Default functioning of the svm.py class
    """
    dataset = None
    if os.path.exists("svm_data.csv"):
        dataset = pandas.read_csv("svm_data.csv")
    else:
        tmp = ImprovedChessDataset("../data/lichess_db_puzzle.csv")
        dataset = tmp.get_representation_for_svm()
        dataset.to_csv("svm_data.csv")

    # Trains on 20000 values - to be changed once k-fold cross validation is finished.
    train_num = 20000
    X = []
    clf = svm.SVC()
    for i in range(train_num):
        x_item = []
        for bit in dataset["FEN"][i][1:-1].split(", "):
            x_item.append(int(bit))
        X.append(x_item)

    Y = []
    for i in range(train_num):
        Y.append(int(dataset["Themes"][i]))

    clf.fit(X, Y)

    # Tests on 30 values - to be changed once k-fold cross validation is finished.
    test_range = 30
    X_test = []
    for i in range(train_num, train_num + test_range):
        x_item = []
        for bit in dataset["FEN"][i][1:-1].split(", "):
            x_item.append(int(bit))
        X_test.append(x_item)

    answers = clf.predict(X_test)
    
    # Prints the accuracy of the model
    correct = 0
    for i in range(test_range):
        actual_theme = dataset["Themes"][i+train_num]
        print(f"Has a predicted theme of {answers[i]}: actual theme {actual_theme}")
        if answers[i] == actual_theme:
            correct+=1

    print(f"Total accuracy: {(correct / test_range) * 100:2.2f}%")


class SVMModel:
    """
    This is the finalized, more organized version of the SVM algorithm
    TO DO: Implement methods given in the evaluation script
    """
    def __init__(self):
        pass


    def __str__(self):
        return "SVMModel"


if __name__ == "__main__":
    main()
