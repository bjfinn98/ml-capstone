import chess
import pandas

def main():
    """
    This main method executes only when this file is called directly.
    """
    test = GameMatrix("../data/lichess_db_puzzle.csv")
    for i in range(2):
        #print(test.convert_game(i))
        for j in test.convert_game(i):
            print(j)

class GameMatrix:
    """
    This class is the adjusted dataset for our project
    We need to adjust the number of classes
    """
    def __init__(self, filename :str):
        """
        The constructor for the GameMatrix

        Deafult piece values:

        Empty Space: 0

        Black King: 1 
        Black Queen: 2
        Black Rooks: 3,4
        Black Bishops: 5,6
        Black Knights: 7, 8
        Black Pawns: 9,10,11,12,13,14,15,16

        White King: 17
        White Queen: 18
        White Rooks: 19,20
        White Bishops: 21,22
        White Knights: 23,24
        White Pawns: 25,26,27,28,29,30,31,32
        """
        self.__dataset = pandas.read_csv(filename)
        self.__dataset.columns=["PuzzleId", "FEN", "Moves", "Rating", "RatingDeviation", "Popularity", "NbPlays", "Themes", "GameUrl"]
        self.__piece_values = {'.': 0, 'p': 9, 'P': 25, 'b': 5, 'B': 21, 'n': 7, 'N': 23, 'r': 3, 'R': 19, 'q': 2, 'Q': 18, 'k': 1, 'K': 17}

    def convert_game(self, n: int):
        """
        This method transforms a board object into an 8x8 matrix of vector
        embeddings. 
        """
        board = chess.Board(self.__dataset['FEN'].iloc[n])
        board_str = str(board)
        print(board_str)
        rows = board_str.split("\n")
        embedded_board = []
        for row in rows: #Iterate through the rows on each chess board
            row_vectors = []
            row = row.replace(' ', '')
            for element in row: #Iterate through each char on a given row
                value = self.__piece_values[element]
                bit_representation = '{0:06b}'.format(value) #Convert the decimal number to a 6 bit string
                row_vectors.append([int(char) for char in bit_representation]) #Convert the string into a list of binary numbers and add to row_vectors
                if (element != '.'): #Add 1 to the value only if it isn't an empty sqaure
                    self.__piece_values[element] = self.__piece_values[element] + 1
            embedded_board.append(row_vectors) #Add the row into the full embedded board
            self.__piece_values = {'.': 0, 'p': 9, 'P': 25, 'b': 5, 'B': 21, 'n': 7, 'N': 23, 'r': 3, 'R': 19, 'q': 2, 'Q': 18, 'k': 1, 'K': 17} #Reset to default values for next puzzle
        return embedded_board

if __name__ == "__main__":
    main()
