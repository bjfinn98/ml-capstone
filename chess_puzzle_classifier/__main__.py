"""
Created By: Barak Finnegan
Date: 2/28/21
Purpose: Showing that poetry is functioning correctly.
"""


import numpy
import scipy
import torch
import pandas
import matplotlib
import chess


def main():
    print(f'Numpy Version: {numpy.__version__}')
    print(f'Scipy version: {scipy.__version__}')
    print(f'Pytorch version: {torch.__version__}')
    print(f'Pandas version: {pandas.__version__}')
    print(f'Matplotlib version: {matplotlib.__version__}')
    print(f'Python-chess version: {chess.__version__}')


if __name__ == "__main__":
    # Checks if this file is target of execution
    main()
