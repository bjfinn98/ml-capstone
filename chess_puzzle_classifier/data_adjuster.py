"""
Created By: Barak Finnegan
Date: 3/3/21
Purpose: Simplifying number of classes to identify
"""

import multiprocessing as mp
import chess
import pandas
import numpy as np
from pprint import pprint
from fen_to_nxn import GameMatrix

def main():
    """
    This main method executes only when this file is called directly.
    """
    board = convert_game("5rk1/1p3ppp/pq3b2/8/8/1P1Q1N2/P4PPP/3R2K1 w - - 2 27")
    print(board)
    
    test = ImprovedChessDataset("../data/lichess_db_puzzle.csv")
    themes = test.get_themes()
    print(f'Number of themes: {len(themes)}')
    pprint(themes)
    data = test.get_representation_for_dffnn()
    print(data.head())


def convert_game(fen: str):
        """
        This method transforms a fen str into an 8x8 matrix of vector
        embeddings.
        """
        piece_values = {'.': 0, 'p': 9, 'P': 25, 'b': 5, 'B': 21, 'n': 7, 'N': 23, 'r': 3, 'R': 19, 'q': 2, 'Q': 18, 'k': 1, 'K': 17}
        board = chess.Board(fen)
        board_str = str(board)
        rows = board_str.split("\n")
        embedded_board = None
        for row in rows: #Iterate through the rows on each chess board
            row = row.replace(' ', '')
            for element in row: #Iterate through each char on a given row
                value = piece_values[element]
                bit_representation = '{0:06b}'.format(value) #Convert the decimal number to a 6 bit string
                vec = np.zeros(6)
                for i, char in enumerate(bit_representation):
                    vec[i] = int(char)

                if embedded_board is None:
                    embedded_board = np.matrix(vec)
                else:
                    embedded_board = np.append(embedded_board, np.matrix(vec), axis=0)

                if (element != '.'): #Add 1 to the value only if it isn't an empty square
                    piece_values[element] = piece_values[element] + 1
    
        return embedded_board

class ImprovedChessDataset:
    """
    This class is the adjusted dataset for our project
    We need to adjust the number of classes
    """
    
    def __init__(self, filename :str, thread=True, verbose=False, convert=True):
        """
        The constructor for the ImprovedChessDataset
        
        Original format: PuzzleId,FEN,Moves,Rating,RatingDeviation,Popularity,NbPlays,Themes,GameUrl

        """
        self.themes_map = { 
                'advancedPawn':'promotion', 'advantage':'advantage',
                'anastasiaMate':'mateInN', 'arabianMate':'mateInN',
                'attackingF2F7':'attack', 'attraction':'REMOVE',
                'backRankMate':'mateInN', 'bishopEndgame':'endgame',
                'bodenMate':'mateInN', 'capturingDefender':'advantage',
                'castling':'defense', 'clearance':'sacrifice',
                'crushing':'advantage', 'defensiveMove':'defense',
                'deflection':'indirect', 'discoveredAttack':'attack',
                'doubleBishopMate':'mateInN', 'doubleCheck':'attack',
                'dovetailMate':'mateInN', 'enPassant':'enPassant',
                'endgame':'endgame', 'equality':'REMOVE',
                'exposedKing':'attack', 'fork':'fork',
                'hangingPiece':'attack', 'hookMate':'mateInN',
                'interference':'interference', 'intermezzo':'intermezzo',
                'kingsideAttack':'attack', 'knightEndgame':'endgame',
                'long':'REMOVE', 'master':'REMOVE',
                'masterVsMaster':'REMOVE', 'mate':'mateInN',
                'mateIn1':'mateInN', 'mateIn2':'mateInN',
                'mateIn3':'mateInN', 'mateIn4':'mateInN',
                'mateIn5':'mateInN', 'middlegame':'indirect',
                'oneMove':'REMOVE', 'opening':'advantage',
                'pawnEndgame':'endgame', 'pin':'pin',
                'promotion':'promotion', 'queenEndgame':'endgame',
                'queenRookEndgame':'endgame', 'queensideAttack':'attack',
                'quietMove':'indirect', 'rookEndgame':'endgame',
                'sacrifice':'sacrifice', 'short':'REMOVE',
                'skewer':'skewer', 'smotheredMate':'mateInN',
                'superGM':'REMOVE', 'trappedPiece':'indirect',
                'underPromotion':'promotion','veryLong':'REMOVE',
                'xRayAttack':'attack', 'zugzwang': 'indirect'}
                
        self.__pandas_set = pandas.read_csv(filename)
        self.__themes = {}
        self.__pandas_set.columns=["PuzzleId", "FEN", "Moves", "Rating", "RatingDeviation", "Popularity", "NbPlays", "Themes", "GameUrl"]
        self.__pandas_set = self.__pandas_set.assign(puzzle_class=lambda x: self.__simplify(x['Themes'], verbose=verbose))
        self.__piece_values = {'.': 0, 'p': 1, 'P': 1, 'b': 3, 'B': 3, 'n': 3, 'N': 3, 'r': 5, 'R': 5, 'q': 9, 'Q': 9, 'k': 100, 'K': 100}
        
        if verbose:
            print("Vec_Emb")

        with mp.Pool(int(mp.cpu_count() * .75)) as pool:
            self.__pandas_set["Vec_Emb"] = pool.map(convert_game, self.__pandas_set["FEN"])
        
        if convert:
            with mp.Pool(int(mp.cpu_count() * .75)) as pool:
                self.__pandas_set["2D_Arr"] = pool.map(self.convert_game_cnn, self.__pandas_set["FEN"])

        vectorize = lambda x: x.Vec_Emb.getA1()
        self.__pandas_set["Vec_Lin"] = self.__pandas_set.apply(vectorize, axis=1)

        self.__themes = {v : k for k, v in self.__themes.items()} # themes is now index to name

    def __simplify(self, themes, verbose=False):
        """
        Taking an input string Series this method will return a string Series based on
        a simplified number of classes selected
        """
        counter = 0
        for i, theme in enumerate(themes):
            new_theme = np.array([0] * 14) # 14 is number of total themes after simplification
            added_themes = set()
            if(i % 10000 == 0 and verbose): print(i)
            for item in theme.split():
                if self.themes_map[item] != "REMOVE" and self.themes_map[item] not in added_themes:
                    if self.themes_map[item] not in self.__themes:
                        self.__themes[self.themes_map[item]] = counter

                        if verbose:
                            print(f"{counter} : {self.themes_map[item]}")
                        counter += 1
                    new_theme[self.__themes[self.themes_map[item]]] = 1
                    added_themes.add(self.themes_map[item])
            
            themes.iat[i] = new_theme
        return themes

    def convert_game_cnn(fen: str):
        """
        This method transforms a fen str into an 8x8 matrix.
        """
        board = chess.Board(fen)
        board_str = str(board)
        rows = board_str.split("\n")
        num_position = np.empty([8, 8])
        i = 0
        for row in rows:
            square_split = row.split(" ")
            sq = map(lambda x: self.__piece_values[x], square_split)
            temp = np.fromiter(sq, dtype=np.int)
            num_position[i] = temp
            i += 1
        return num_position


    def print_head(self, n: int):
        """
        This method helps gets a sense of the top of the dataset
        n: int specifying number of rows to print
        """
        print(self.__pandas_set.head(n))


    def np_to_themes(self, x):
        names = []
        for i, j in enumerate(x):
            if j == 1:
                names.append(self.__themes[i])
        return names

    def get_themes_dict(self):
        return self.__themes

    def get_themes(self):
        """
        This method will get all unique puzzle themes in our dataset
        """
        themes = set()
        for item in self.__pandas_set["Themes"]:
            for i, theme in enumerate(item):
                if theme == 1:
                    themes.add(self.__themes[i])
        return themes
    

    def get_data(self, model_name: str):
        mapping = {"SVMModel": self.get_representation_for_svm,
                   "DFFNNModel": self.get_representation_for_dffnn,
                   "CNNModel": self.get_representation_for_cnn}

        if model_name not in mapping:
            return None
        return mapping[model_name]
    
    def get_representation_for_svm(self):
        """
        Need to convert FEN to vector embedding with James' algorithm
        """
        return self.__pandas_set[["Vec_Emb", "Themes"]]


    def get_representation_for_dffnn(self):
        return self.__pandas_set[["Vec_Lin", "Themes"]]


    def get_representation_for_cnn(self):
        return self.__pandas_set[["2D_Arr", "Themes"]]

if __name__ == "__main__":
    main()
