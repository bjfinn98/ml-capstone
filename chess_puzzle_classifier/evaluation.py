from svm import SVMModel
from dffnn import DFFNNModel
from cnn import CNNModel
from abc import ABCMeta 
from data_adjuster import ImprovedChessDataset
import os
import torch
import numpy as np

def main():
    tmp = [DFFNNModel(), SVMModel()]
    models = []
    
    for model in tmp:
        if not issubclass(model, Model):
            print(f"{model} does not implement all the required methods of the Model interface.")
        else:    
            models.append(model)

    if len(models) == 0:
        print("No models have been implemented to evaluate")
        exit(1) 

    print(f"We have {len(models)} models to test")

    
    # Speed up loading time of the dataset
    dataset = None
    print("Loading dataset")
    if os.path.exists("evaluation.csv"):
        dataset = pandas.read_csv("evaluation.csv")
    else:
        dataset = ImprovedChessDataset("../data/lichess_db_puzzle.csv", convert=False)
   
    print("Data loaded")
    # have some models to evaluate
    for model in models:
        print(f"Evaluating {model}...")

        data = dataset.get_representation_for_dffnn()

        #implement k-fold and boosting here in the future perhaps
        split = int(.8 * len(data))

        #prelimanary_training
        print("Loading model")
        if os.path.exists("dffnn.nn"):
            model.load_state_dict(torch.load("dffnn.nn"))
        else:
            model.train(data[0:split])

        # with trained model we can predict new values

        correct = 0
        for test_ele in zip(data["Themes"], data["Vec_Lin"]):
            if test_ele[0][np.argmax(model.predict(np.fromstring(test_ele[1])))] == 1:
                correct += 1

        print(f"{model}: {(correct / len(data)) * 100}% accuracy")
        print(f"{correct}, {len(data)}")
        


class Model(metaclass=ABCMeta):
    """
    The interface that each of our models need to implement
    """
    @classmethod
    def __subclasshook__(cls, subclass):
        # Train needs to take a dataframe portion of data points and actual results
        # Predict takes one data point predicts classes with probability vector
        return (hasattr(subclass, 'train') and callable(subclass.train) and
                hasattr(subclass, 'predict') and callable(subclass.predict))


if __name__ == "__main__":
    main()
