import chess
import pandas
import numpy as np

def main():
    """
    This main method executes only when this file is called directly.
    """
    test = GameMatrix("../data/lichess_db_puzzle.csv")
    for i in range(10):
        print(test.convert_game(i))

class GameMatrix:
    """
    This class transforms board objects from FEN Notation to a 2D Matrix representation.
    """
    def __init__(self, filename :str):
        """
        The constructor for the GameMatrix
        """
        self.__dataset = pandas.read_csv(filename)
        self.__dataset.columns=["PuzzleId", "FEN", "Moves", "Rating", "RatingDeviation", "Popularity", "NbPlays", "Themes", "GameUrl"]
        self.__piece_values = {'.': 0, 'p': 1, 'P': 1, 'b': 3, 'B': 3, 'n': 3, 'N': 3, 'r': 5, 'R': 5, 'q': 9, 'Q': 9, 'k': 100, 'K': 100}


    def convert_game(self, n: int):
        """
        This method transforms a board object into a 8x8 matrix of numbers
        n: row in original puzzle dataset for which to transform the puzzle
        """
        board = chess.Board(self.__dataset['FEN'].iloc[n])
        board_str = str(board)
        rows = board_str.split("\n")
        num_position = np.empty([8, 8])
        i = 0
        for row in rows:
            square_split = row.split(" ")
            sq = map(lambda x: self.__piece_values[x], square_split)
            temp = np.fromiter(sq, dtype=np.int)
            num_position[i] = temp
            i+=1
        return num_position

if __name__ == "__main__":
    main()
