"""
Created By: Tanvi Haldankar
Date: 3/29/21
Purpose: Create a stratified k-fold cross-validation dataset
"""

import chess
import pandas
from data_adjuster import ImprovedChessDataset
from sklearn.model_selection import StratifiedKFold

def main():
    """
    This main method executes only when this file is called directly.
    """
    dataset = ImprovedChessDataset("../data/lichess_db_puzzle.csv")
    dataset = StratifiedKFoldDataset(dataset)
    dataset.dataset_stats()


class StratifiedKFoldDataset:
    """
    This class splits the dataset for Stratified K-Fold Cross Validation.
    """
    def __init__(self, dataset: ImprovedChessDataset):
        """
        The constructor for the KFoldDataset
        
        Takes in an ImprovedChessDataset object to get information about puzzle themes.
        """     
        self.__pandas_set = dataset
        self.__pandas_set.columns=["PuzzleId", "FEN", "Moves", "Rating", "RatingDeviation", "Popularity", "NbPlays", "Themes", "GameUrl"]
        makeClassifications()


    def toBinary(x):
        """
        This method converts every unique theme vector into a number. This allows
        us to eventually split the dataset off of themes.
        """  
        x = x.reshape(2, 7)
        temp = [ np.packbits(y) for y in x ]
        return sum(temp)[0]


    def makeClassifications():
        """
        This method appends a column to the ImprovedChessDataset
        with classification information.
        """  
        classification_vectors = self.__pandas_set['puzzle_class']
        y = classification_vectors.apply(toBinary)
        self.__pandas_set['k_fold_classification'] = y


    def print_head(self, n: int):
        """
        This method helps gets a sense of the top of the dataset
        n: int specifying number of rows to print
        """
        print(self.__pandas_set.head(n))
        print(self.__pandas_set.Themes.value_counts())
    

    def dataset_stats(self):
        """
        This method prints information about the dataset.
        The comment below shows a counter for each of the themes that exist
        in the dataset.
        """
        svm_rep = self.__pandas_set.get_representation_for_svm()
        ser = svm_rep.Themes.str.split()
        order_dict = {}
        for i in ser:
            order_dict = Counter(order_dict) + Counter(i)
        print(order_dict)

    """ Counter({'0': 1,128,756, -- advantage
                 '2': 818,960, -- endgame
                 '1': 629,196, -- indirect
                 '4': 314,551, -- promotion
                 '6': 306.661, -- attack
                 '8': 231,913, -- pin
                 '10': 101,127, -- mateInN
                 '5': 98,554, -- skewer
                 '9': 75,811, -- fork
                 '3': 63,308, -- defense
                 '7': 31,985, -- sacrifice
                 '11': 24,286, -- enPassant
                 '12': 6,117, -- interference
                 '13': 2,056} -- intermezzo
                )
    """

    
    def create_kfolds(self, num_splits):
        """
        This method creates Stratified K-Folds of the dataset
        by stratifying by the puzzle themes vector. A parameter
        is passed in for how many splits are created, and then
        a matrix with num_splits rows is returned, storing the 
        input training, input testing, output training, and
        output testing data.
        """
        skf = StratifiedKFold(n_splits=num_splits, random_state=None, shuffle=False)
        X = df.FEN.to_numpy()
        y = df.Classification.to_numpy()
        folds = []
        for train_index, test_index in skf.split(X, y):
            # helpful to print the training vs. testing split
            # print("TRAIN:", train_index, "TEST:", test_index)  
            X_train, X_test = X[train_index], X[test_index]
            y_train, y_test = y[train_index], y[test_index]
            temp = np.array([X_train, X_test, y_train, y_test])
            folds.append(temp)
        return folds


if __name__ == "__main__":
    main()
