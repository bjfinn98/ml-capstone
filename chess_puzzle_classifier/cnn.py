"""
Created By: Tanvi Haldankar
Date: 3/29/2021
Purpose: Create and train a Convolutional Neural Network
"""
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import numpy as np
import pandas
from data_adjuster import ImprovedChessDataset
from fen_to_nxn import GameMatrix
from matplotlib import pyplot as plt

def main():
    """
    This main method executes only when this file is called directly.
    """
    test = CNNModel()
    dataset = ImprovedChessDataset("../data/lichess_db_puzzle.csv") # get themes for each puzzle
    gm = GameMatrix("../data/lichess_db_puzzle.csv") # get 2D Array representation for input board
    test.train(gm, dataset, 5, 500000, 2000)

    # predict
    #for(i in range())
    pred = torch.tensor(gm.convert_game(25000))
    pred = pred.reshape(1,1,8,8)
    print(test.predict(pred))
    res = tmp._ImprovedChessDataset__pandas_set.iloc[25000].Themes
    print(f"Expected: {res}")


class CNNModel(nn.Module):
    """
    This is the finalized implementation for the CNN model
    """
    def __init__(self):
        super(NeuralNet, self).__init__()
        self.conv1 = nn.Conv2d(1, 8, 2, 1)
        self.conv2 = nn.Conv2d(8, 16, 2, 1)
        self.conv3 = nn.Conv2d(16, 32, 2, 1)
        self.conv4 = nn.Conv2d(32, 64, 2, 1)
        self.fc1 = nn.Linear(256, 128)
        self.fc2 = nn.Linear(128, 64)
        self.fc3 = nn.Linear(64, 32)
        self.fc4 = nn.Linear(32, 14)

        self.loss_fn = nn.MultiLabelMarginLoss()
        self.optimizer = optim.SGD(self.parameters(), lr=0.001, momentum=0.9)
    

    def forward(self,x):
        """
        Used to train the model
        """
        x = self.conv1(x)
        x = F.tanh(x)
        x = self.conv2(x)
        x = F.tanh(x)
        x = self.conv3(x)
        x = F.tanh(x)
        x = self.conv4(x)
        x = F.relu(x)
        x = F.max_pool2d(x,2)
        x = torch.flatten(x, 1)
        x = self.fc1(x)
        x = self.fc2(x)
        x = self.fc3(x)
        x = self.fc4(x)
        output = F.softmax(x, dim=1)
        return output


    def train(self, gm: GameMatrix, dataset: ImprovedChessDataset, num_epoch, num_points, snapshot_length):
        """
        Training script that goes through dataset. It creates tensors from the 2D 
        representation of a chess board and compares against the given output
        vector. The loss for every 1000 datapoints trained on is saved in the loss_rate
        array and can be graphed through the graph_loss function.
        """
        loss_rate = []
        for epoch in range(num_epoch):
            running_loss = 0.0
            
            for i in range(0, num_points):
                tensor = torch.tensor(gm.convert_game(i))
                tensor = tensor.reshape(1,1,8,8)
                output = model(tensor.float())
                model.train()
                output_tensor = torch.tensor(dataset.iloc[i].Themes).type(torch.FloatTensor)
                output_tensor = output_tensor.reshape(1, 14)

                loss = loss_fn(output, output_tensor)
                
                optimizer.zero_grad()
                
                loss.backward()
                optimizer.step()
                running_loss += loss.item()

                if i % snapshot_length == 0:
                    print('[%d, %5d] loss: %.3f' %
                            (epoch + 1, i, running_loss / snapshot_length))
                    loss_rate.append(running_loss / snapshot_length)
                    print(output)
                    print(output_tensor)
                    running_loss = 0.0

        graph_loss(loss_rate, snapshot_length)

    
    def graph_loss(loss_rate, snapshot_length):
        """
        Graphs the training loss for the Convolutional Neural Network.
        An array with snapshots of loss rates is passed in and is graphed.
        """
        loss_rate = [x*100 for x in loss_rate]
        training_rate = [snapshot_length*i for i in range(len(loss_rate))]
        plt.plot(training_rate, loss_rate)
        plt.xlabel("Number of Puzzles (s)")
        plt.ylabel("Loss (%)")
        plt.title("Training Loss")

    
    def predict(self, inp):
        """
        Handles predicting output from a test tensor.
        """
        x = torch.tensor(inp).float()
        x = self.conv1(x)
        x = F.tanh(x)
        x = self.conv2(x)
        x = F.tanh(x)
        x = self.conv3(x)
        x = F.tanh(x)
        x = self.conv4(x)
        x = F.relu(x)
        x = F.max_pool2d(x,2)
        x = torch.flatten(x, 1)
        x = self.fc1(x)
        x = self.fc2(x)
        x = self.fc3(x)
        x = self.fc4(x)
        output = F.softmax(x, dim=1)
        return output

    def __str__(self):
        return "CNNModel"

if __name__ == "__main__":
    main()
