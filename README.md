# ML-Capstone

This tool is used to predict what category a chess position is in terms of a chess puzzle.  

# Setup #
In rlogin use:  

```bash
python3 get-poetry.py  
```

After you install poetry, you should be ready to go with the preconfigured lock file.  
After restarting your session, you can test this by:  

```bash
poetry install
poetry run python3 chess_puzzle_classifier 
```

This should specify the versions of all our dependencies


I recommend adding the following to .bashrc:
alias py="poetry run python3"

Any of the scripts can be run like this:

```bash
poetry run python3 chess_puzzle_classifier/<script_name>
```

To run a Jupyter Notebook, use the following:
```bash
poetry shell
jupyter notebook
```

# Dataset #

Need to decompress the lichess puzzle dataset with:

```bash
bzip2 -dk ./data/lichess_db_puzzle.csv.bz2
```

# Branching #

All the branching should be done off of the development branch. You can checkout that branch and create your own branch for experimenting/feature creating.


# Authors #
Barak Finnegan, James Park, Tanvi Haldankar  


# Setup Contributions #

README.md author: Barak Finnegan  
poetry.lock author: Barak Finnegan  
pyproject.toml author: Barak Finnegan  
get-poetry.py author: Poetry developers  
Other contributions will be noted in source files.